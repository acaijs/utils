<p align="center"><img src="https://api.aposoftworks.com/storage/image/ehRdFIz6tqiERXID1SIXAeu0mmTBKLdixIXsNj9s.png" width="256"></p>

# Açai Utils Module

[![Build Status](https://travis-ci.org/AcaiFramework/presenter.svg?branch=production)](https://travis-ci.org/AcaiFramework/presenter) [![Support](https://img.shields.io/badge/Patreon-Support-orange.svg?logo=Patreon)](https://www.patreon.com/rafaelcorrea)

A set of tools used by the Açaí Framework to enhance and ease your development experience.

## Utils available:
- presenter
- custom exception